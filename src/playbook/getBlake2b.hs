import Data.String
import System.Environment

main :: IO ()
main = do
  inData <- fmap head $ getArgs
  let x = (!! 3) $ head $ filter pred' $ map words $ lines inData
  putStrLn x

pred' ("Raw":"Blake2b":"hash:":xs) = True
pred' _ = False
