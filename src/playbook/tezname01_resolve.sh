#!/bin/bash
#
if [ "$1" == "" ]; then
  basedir="."
else
  basedir="$1"
fi
#
srcdir="${basedir}/src"
scriptdir="${srcdir}/playbook"
contractdir="${srcdir}/contract"
logdir="${basedir}/log"
version=$(liquidity --version) || exit 1
#
version=0.3
# alphanode='alphanet.api.tezos.id'
alphanode="node2.sg.tezos.org.sg"
yunyan0=tz1ZGfRgUiPVRC7qEnJ5WNwUDXQGaZEjZCwH
admin="${yunyan0}"
#
source "${basedir}/ADDR"  || exit 1
echo "load ${basedir}/ADDR"
#
tezname="yunyan"
#
# ===========================
# ===== resolve tezname =====
# ===========================
#
arg="(Right (Right (Left "
arg+="(Pair \"${ROOTNAMEADDR}\" " # _parentAddr : address
arg+="\"${ADDR01}\" )" # _targetNameAddr : address
arg+=")))"
#
# tezos-client -S -P 443 -A "${alphanode}" \
tezos-client -A "${alphanode}" \
  transfer 0 from "${admin}" \
  to "${GATEADDR}" \
  --arg "${arg}" \
  --burn-cap 16.0 \
  > "${logdir}/${nameAddr}.update" 2>&1 || exit 1
echo -e "${tezname} resolved"
#
