#!/bin/bash
#
if [ "$1" == "" ]; then
  basedir=$(git rev-parse --show-toplevel) || exit 1
else
  basedir="$1"
fi
echo "[info] set basedir as ${basedir}"
#
echo "[info] loading ${basedir}/ENV.."
source "${basedir}/ENV" || exit 1
#
echo "[info] reseting ${basedir}/ADDR.."
if [ -f "${basedir}/ADDR" ]; then
  rm "${basedir}/ADDR"
  touch "${basedir}/ADDR"
else
  touch "${basedir}/ADDR"
fi
#
# =====================
# ===== nametypes =====
# =====================
#
cname=cNameTypes
#
initS=""
# setup admin address
initS+="(Pair \"${admin}\""
# setup the root byte of premium merkle tree
initS+="(Pair 0x05ea57164837f0314f1eeda6ff84a3e54fa85e69cf11adaece6def27239ab971 "
# setup the root byte of restricted merkle tree
initS+="0xc12823a002f9c62d0ec17fa65b1585e9b8af51465ca7fbfb350bbd1234c4c58e))"
#
echo "[info] deploying ${cname}-${version}.."
#
tezos-client -A "${alphanode}"\
  originate contract "${cname}-${version}" \
  for "$admin" \
  transferring 0 from "$admin" \
  running "${contractdir}/${cname}.tz" \
  --init "$initS" \
  --burn-cap 16.0 \
  --force \
  > "${logdir}/${cname}.deployment" 2>&1 || exit 1
sleep 1
#
nameTypesAddr=$(stack runghc -- "${scriptdir}/contractAddressExtracter.hs" "${logdir}/${cname}.deployment") || exit 1
echo "NAMETYPESADDR=${nameTypesAddr}" >> "${basedir}/ADDR"
echo -e "[info] \033[33m$cname\033[39m has been originated at \033[32m$nameTypesAddr\033[39m"
#
# ====================
# ===== rootname =====
# ====================
#
cname=cTezname
#
# the raw Blake2b hash of "tez"
rootname=0xfa73f2049206617fdd78d682c1605ec4cecaa3dffee7a069d4cb97ce40fb679b
#
# setup default tri-prices for rootname
fee1=5
fee2=10
fee3=20
#
initS=""
initS+="(Pair ${rootname} "    # :tHashedName
initS+="(Pair \"${admin}\" "   # sAdmin
# --
initS+="(Pair \"${yunyan0}\" " # sDest
initS+="(Pair \"${yunyan0}\" " # sOwner
# --
initS+="(Pair (Right (Right $rootname)) " # sTeznameType
initS+="(Pair (Pair ${fee1}000000 (Pair ${fee2}000000 ${fee3}000000)) " # sFees
initS+="(Pair {} "                        # sSubnameRecords
# --
initS+="(Pair \"2019-10-01T00:00:01Z\" "  # sRegistrationDate
initS+="(Pair \"2119-10-01T00:00:00Z\" "  # sExpirationDate
initS+="(Pair \"2019-10-01T00:00:00Z\" \"GENESIS\"" # sLastModification
initS+="))))))))))"
#
echo "[info] deploying ${cname}-${version}.."
#
tezos-client -A "${alphanode}"\
  originate contract "rootname-${version}" \
  for "$admin" \
  transferring 0 from "$admin" \
  running "${contractdir}/${cname}.tz" \
  --init "$initS" \
  --burn-cap 16.0 \
  --force \
  > "${logdir}/rootname.deployment" 2>&1 || exit 1
sleep 1
rootnameAddr=$(stack runghc -- "${scriptdir}/contractAddressExtracter.hs" "${logdir}/rootname.deployment") || exit 1
echo "ROOTNAMEADDR=${rootnameAddr}" >> "${basedir}/ADDR"
echo -e "[info] \033[33mrootname\033[39m has been originated at \033[32m$rootnameAddr\033[39m"
#
# ================
# ===== gate =====
# ================
#
cname=cGate
#
comm=3 # (commission in xtz)
#
initS=""
initS+="(Pair \"$yunyan0\" "       # sAdmin
initS+="(Pair \"$nameTypesAddr\" " # scNameTypes
initS+="(Pair \"$rootnameAddr\" "  # scRootname
initS+="(Pair ${comm}000000 " # sCommission as in mutez
# sTaxPc as 10%
initS+="(Pair 10 "
# sDuration as 1 mins
# PS : 3600 = 1hr | 86400 = 24hr
initS+="(Pair 60 "
initS+="{}))))))"       # sAuctions
#
echo "[info] deploying ${cname}-${version}.."
#
tezos-client -A "${alphanode}"\
  originate contract "$cname-${version}" \
  for "$admin" \
  transferring 0 from "$admin" \
  running "${contractdir}/${cname}.tz" \
  --init "$initS" \
  --burn-cap 16.0 \
  --force \
  > "${logdir}/${cname}.deployment" 2>&1 || exit 1
sleep 1
gateAddr=$(stack runghc -- "${scriptdir}/contractAddressExtracter.hs" "${logdir}/${cname}.deployment") || exit 1
echo "GATEADDR=${gateAddr}" >> "${basedir}/ADDR"
echo -e "[info] \033[33m$cname\033[39m has been originated at \033[32m$gateAddr\033[39m"
#
echo -e "[info] deployment completed"
