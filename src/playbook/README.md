Deploying the entire teznames system as in demo requires the following steps:

**step 1**

```bash
./sysdep.sh
```

to deploy contracts: the _gate_, the _nameTypes_ and the _rootname_.

**step 2**

```bash
./basenames.sh
```

to deploy 5 _level-1_ teznames. Please notice that this will only deploy them. The owner of all of them will still be the system admin. 
